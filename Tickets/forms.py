from django import forms
from .models import Ticket, Settings

qty_choices = (
    (1,1),
    (2,2),
    (3,3),
    (4,4),
    (5,5),
    (6,6),
    (7,7),
    (8,8),
    (9,9),
    (10,10)    
)

class TicketForm(forms.ModelForm):
    class Meta:
        model = Ticket
        fields = [
            'id',
            'couponcode',
            'activateddate',
            'activatednotes',
            'redeemeddate',
            'voiddate',
            'voidnotes',
            'ticketgroup'
        ]

class RedeemTicketForm(forms.ModelForm):
    couponcode = forms.CharField(
        max_length=11,
        required=False,
        label='Scan/Enter Code',
        widget=forms.TextInput(attrs={
            'placeholder': '1A2B3C4D',
            'autofocus': 'true'
        }))
    id = forms.IntegerField(
        label='Ticket Number',
        required=False,
        widget=forms.TextInput(attrs={
            'placeholder': '2001 (optional)'
        }))
    
    class Meta:
        model = Ticket
        fields = [
            'couponcode',
            'id'
        ]


class GroupRedeemForm(forms.ModelForm):
    class Meta:
        model = Ticket
        fields = [
            'couponcode'
        ]
        widgets = {
            'couponcode': forms.HiddenInput()
        }


class SellTicketForm(forms.ModelForm):
    quantity = forms.ChoiceField(choices=qty_choices, initial=(1,1))
    activatednotes = forms.CharField(
        max_length=255,
        required=False,
        label='Note',
        widget=forms.TextInput(attrs={
            'placeholder': 'optional',
            'autofocus': 'true'
        }))

    class Meta:
        model = Ticket
        quantity = forms.ChoiceField()
        fields = [
            'activatednotes'
        ]


class ActivateTicketForm(forms.ModelForm):
    couponcode = forms.CharField(
        max_length=11,
        label='Scan/Enter Code',
        widget=forms.TextInput(attrs={
            'placeholder': '1A2B3C4D',
            'autofocus': 'true'
        }))
    activatednotes = forms.CharField(
        max_length=255,
        label='Note',
        required=False,
        widget=forms.TextInput(attrs={
            'placeholder': 'optional'
        }))
    
    class Meta:
        model = Ticket
        fields = [
            'id',
            'couponcode',
            'activateddate',
            'activatednotes',
            'redeemeddate',
            'voiddate',
            'voidnotes',
            'ticketgroup',
            'ticketbundle'
        ]
        widgets = {
            'id': forms.HiddenInput(),
            'activateddate': forms.HiddenInput(),
            'redeemeddate': forms.HiddenInput(),
            'voiddate': forms.HiddenInput(),
            'voidnotes': forms.HiddenInput(),
            'ticketgroup': forms.HiddenInput(),
            'ticketbundle': forms.HiddenInput()
        }
    

class VoidTicketForm(forms.ModelForm):
    couponcode = forms.CharField(
        max_length=11,
        label='Scan/Enter Code',
        required=False,
        widget=forms.TextInput(attrs={
            'placeholder': '1A2B3C4D',
            'autofocus': 'true'
        }))
    id = forms.IntegerField(
        label='Ticket Number',
        required=False,
        widget=forms.TextInput(attrs={
            'placeholder': '3001'
        }))
    voidnotes = forms.CharField(
        max_length=255,
        label='Notes',
        required=False,
        widget=forms.TextInput(attrs={
            'placeholder': 'optional'
        }))
    
    class Meta:
        model = Ticket
        fields = [
            'id',
            'couponcode',
            'activateddate',
            'activatednotes',
            'redeemeddate',
            'voiddate',
            'voidnotes',
            'ticketgroup'
        ]
        widgets = {
            'activateddate': forms.HiddenInput(),
            'activatednotes': forms.HiddenInput(),
            'redeemeddate': forms.HiddenInput(),
            'voiddate': forms.HiddenInput(),
            'ticketgroup': forms.HiddenInput()
        }
    

class SettingsForm(forms.ModelForm):
    groupinterval = forms.IntegerField(min_value=0, label='Seconds between groups')
    class Meta:
        model = Settings
        fields = [
            'groupinterval'
        ]


# class VoidTicketForm(forms.Form):
#     couponcode = forms.CharField(max_length=11, required=False)
#     id = forms.IntegerField(required=False)