from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.core.paginator import Paginator
from .forms import TicketForm, RedeemTicketForm, VoidTicketForm, SellTicketForm, ActivateTicketForm, GroupRedeemForm, SettingsForm
from .models import Ticket, Settings
from django.urls import reverse
from django.contrib import messages
from django.db import connection
from django.db.models import Max
from datetime import datetime


# timezone.now()
now = datetime.now()
current_timestamp = now.strftime('%Y-%m-%d %H:%M:%S')

sell_history = [['initialized', 'info'],]
activate_history = [['initialized', 'info'],]
bactivate_history = [['initialized', 'info'],]
redeem_history = [['initialized', 'info'],]
void_history = [['initialized', 'info'],]


def add_to_list(list, msg, msg_level):
    if len(list) < 10:
        list.insert(0, [msg, msg_level])
    else:
        list.pop()
        list.insert(0, [msg, msg_level])


def handle_message(context, msg, msg_level, msg_list):
    print(msg)
    m_level = msg_level
    if m_level == 'error':
        messages.add_message(context, messages.ERROR, msg)
    elif m_level == 'warning':
        messages.add_message(context, messages.WARNING, msg)
    elif m_level == 'info':
        messages.add_message(context, messages.INFO, msg)
    elif m_level == 'success':
        messages.add_message(context, messages.SUCCESS, msg)
    add_to_list(msg_list, msg, m_level)


def ticket_create_view(request):
    my_form = TicketForm(request.POST or None)
    if form.is_valid():
        form.save()
    context = {
        'form': my_form
    }
    return render(request, 'tickets/ticket_create.html', context)
    # return reverse('create')


def ticket_detail_view(request, tid):
    obj = get_object_or_404(Ticket, id=tid)
    context = {
        'object': obj
    }
    return render(request, 'tickets/ticket_detail.html', context)
    # return reverse('detail')


def ticket_list_view(request):
    queryset = Ticket.objects.all()
    page = int(request.GET.get('page', 1))
    paginator = Paginator(queryset, 15)
    num_pages = paginator.num_pages
    # current_page = context.get('page')
    # page_no = current_page.number
    if num_pages <= 11 or page <= 6:
        pages = [x for x in range(1, min(num_pages + 1, 12))]
    elif page > num_pages - 6:
        pages = [x for x in range(num_pages - 10, num_pages + 1)]
    else:
        pages = [x for x in range(page - 5, page + 6)]
    try:
        tickets = paginator.page(page)
    except PageNotInteger:
        tickets = paginator.page(1)
    except EmptyPage:
        tickets = paginator.page(num_pages)
    context = {
        'page': page,
        'pages': pages,
        'tickets': tickets
    }
    return render(request, 'tickets/ticket_list.html', context)


def ticket_redeem(request):
    my_form = RedeemTicketForm()
    settings = Settings.objects.get(id=1)
    timer = settings.groupinterval
    queryset = None
    queryset_len = 0
    group_notes = ''
    if request.method == 'POST':
        ############## need to implement this check ###################
        # try:
        #     this_ticket = Ticket.objects.get(couponcode=this_code)
        #     my_form = ActivateTicketForm(request.POST, instance=this_ticket)
        # except:
        #     msg = f'Code {this_code} not found.'
        #     print(msg)
        #     messages.add_message(request, messages.ERROR, msg)
        #     add_to_list(activate_history, msg, 'error')
        if request.POST.get('Redeem'):
            this_code = request.POST.get('couponcode', None)
            this_ticket = Ticket.objects.get(couponcode=this_code)
            if this_ticket.ticketgroup:
                queryset = Ticket.objects.filter(ticketgroup=this_ticket.ticketgroup)
                queryset_len = queryset.count()
            group_notes = this_ticket.activatednotes
            my_form = RedeemTicketForm(request.POST, instance=this_ticket)
            if my_form.is_valid():
                if queryset_len < 2:
                    # this_ticket.refresh_from_db()
                    if this_ticket.voiddate:
                        msg = f'Ticket marked VOID on {this_ticket.voiddate}. Note: {this_ticket.voidnotes}'
                        print(msg)
                        messages.add_message(request, messages.ERROR, msg)
                        add_to_list(redeem_history, msg, 'error')
                        return HttpResponseRedirect('')
                    elif this_ticket.redeemeddate:
                        msg = f'Ticket {this_ticket.id} ({this_ticket.couponcode}) already REDEEMED on {this_ticket.redeemeddate}.'
                        print(msg)
                        add_to_list(redeem_history, 'error')
                        return HttpResponseRedirect('')
                    elif not this_ticket.activateddate:
                        msg = f'Code {this_code} not yet activated.'
                        print(msg)
                        messages.add_message(request, messages.ERROR, msg)
                        add_to_list(redeem_history, msg, 'error')
                        return HttpResponseRedirect('')
                    elif this_ticket.activateddate:
                        this_ticket.redeemeddate = current_timestamp
                        this_ticket.save(update_fields=['redeemeddate'])
                        msg = f'Ticket {this_ticket.id} ({this_ticket.couponcode}) REDEEMED at {current_timestamp}.'
                        print(msg)
                        messages.add_message(request, messages.SUCCESS, msg)
                        add_to_list(redeem_history, msg, 'success')
                        return HttpResponseRedirect('')
        elif request.POST.get('Only One'):
            this_code = request.POST.get('couponcode', None)
            this_ticket = Ticket.objects.get(couponcode=this_code)
            if this_ticket.ticketgroup:
                queryset = Ticket.objects.filter(ticketgroup=this_ticket.ticketgroup)
                queryset_len = queryset.count()
            group_notes = this_ticket.activatednotes
            my_form = RedeemTicketForm(request.POST, instance=this_ticket)
            if my_form.is_valid():
                # this_ticket.refresh_from_db()
                if this_ticket.voiddate:
                    msg = f'Ticket marked VOID on {this_ticket.voiddate}. Note: {this_ticket.voidnotes}'
                    print(msg)
                    messages.add_message(request, messages.ERROR, msg)
                    add_to_list(redeem_history, msg, 'error')
                    return HttpResponseRedirect('')
                elif this_ticket.redeemeddate:
                    msg = f'Ticket {this_ticket.id} ({this_ticket.couponcode}) already REDEEMED on {this_ticket.redeemeddate}.'
                    print(msg)
                    messages.add_message(request, messages.ERROR, msg)
                    add_to_list(redeem_history, msg, 'error')
                    return HttpResponseRedirect('')
                elif not this_ticket.activateddate:
                    msg = f'Code {this_code} not yet activated.'
                    print(msg)
                    messages.add_message(request, messages.ERROR, msg)
                    add_to_list(redeem_history, msg, 'error')
                    return HttpResponseRedirect('')
                elif this_ticket.activateddate:
                    this_ticket.redeemeddate = current_timestamp
                    this_ticket.save(update_fields=['redeemeddate'])
                    msg = f'Ticket {this_ticket.id} ({this_ticket.couponcode}) REDEEMED at {current_timestamp}.'
                    print(msg)
                    messages.add_message(request, messages.SUCCESS, msg)
                    add_to_list(redeem_history, msg, 'success')
                    return HttpResponseRedirect('')
        elif request.POST.get('Whole Group'):
            this_code = request.POST.get('couponcode', None)
            this_ticket = Ticket.objects.get(couponcode=this_code)
            if this_ticket.ticketgroup:
                this_group = this_ticket.ticketgroup
                queryset = Ticket.objects.filter(ticketgroup=this_ticket.ticketgroup)
                queryset_len = queryset.count()
            group_notes = this_ticket.activatednotes
            my_form = RedeemTicketForm(request.POST, instance=this_ticket)
            valid_group = False
            if my_form.is_valid():
                for each in queryset:
                    if each.voiddate:
                        msg = f'Ticket marked VOID on {each.voiddate}. Note: {each.voidnotes}'
                        print(msg)
                        messages.add_message(request, messages.ERROR, msg)
                        add_to_list(redeem_history, msg, 'error')
                        # return HttpResponseRedirect('')
                    elif each.redeemeddate:
                        msg = f'Ticket {each.id} ({each.couponcode}) ALREADY redeemed on {each.redeemeddate}.'
                        print(msg)
                        messages.add_message(request, messages.ERROR, msg)
                        add_to_list(redeem_history, msg, 'error')
                        # return HttpResponseRedirect('')
                    elif not each.activateddate:
                        msg = f'Code {this_code} not yet activated.'
                        print(msg)
                        messages.add_message(request, messages.ERROR, msg)
                        add_to_list(redeem_history, msg, 'error')
                        # return HttpResponseRedirect('')
                    elif each.activateddate:
                        msg = f'Ticket {each.id} ({each.couponcode}) REDEEMED at {current_timestamp}.'
                        print(msg)
                        messages.add_message(request, messages.INFO, msg)
                        valid_group = True
                        # return HttpResponseRedirect('')
                if valid_group:
                    queryset.update(redeemeddate=current_timestamp)
                    msg = f'Group of {queryset_len} REDEEMED at {current_timestamp}.'
                    print(msg)
                    messages.add_message(request, messages.SUCCESS, msg)
                    add_to_list(redeem_history, msg, 'success')
                    return HttpResponseRedirect('')
    context = {
        "form": my_form,
        'ticket_group': queryset,
        'ticket_group_len': queryset_len,
        'group_notes': group_notes,
        'history': redeem_history,
        'timer': timer
    }
    return render(request, 'tickets/redeem.html', context)


def get_next_group():
    highest_ticket = Ticket.objects.aggregate(Max('ticketgroup'))
    highest_group = highest_ticket['ticketgroup__max']
    if not highest_group:
        return 1
    else:
        return highest_group + 1


def get_next_code():
    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM GenerateCode(8)')
        next_code = cursor.fetchone()[0]
        return next_code


def sell_tickets(request, *args, **kwargs):
    my_form = SellTicketForm(request.POST or None)
    if request.method == 'POST':
        if my_form.is_valid():
            this_qty = int(my_form.cleaned_data.get('quantity'))
            this_qty_str = my_form.cleaned_data.get('quantity')
            this_notes = my_form.cleaned_data.get('activatednotes')
            try:
                next_group = get_next_group()
                while this_qty > 0:
                    next_code = get_next_code()
                    Ticket.objects.create(couponcode=next_code,activateddate=current_timestamp,activatednotes=this_notes)
                    this_qty -= 1
                msg = f'Sold {this_qty_str} tickets, group: {next_group}. Activated: {current_timestamp}. Note: {this_notes}.'
                # handle_message(context, msg, 'success', redeem_history)
                print(msg)
                messages.add_message(request, messages.SUCCESS, msg)
                add_to_list(sell_history, msg, 'success')
                return HttpResponseRedirect('')
            except Exception as err:
                msg = f'Error: {err}'
                # handle_message(context, msg, 'error', redeem_history)
                print(msg)
                messages.add_message(request, messages.ERROR, msg)
                add_to_list(sell_history, msg, 'error')
                return HttpResponseRedirect('')
            
    context = {
        'form': my_form,
        'history': sell_history
    }
    return render(request, 'tickets/sell.html', context)


# def activate_tickets(request, *args, **kwargs):
#     my_form = ActivateTicketForm()
#     if request.method == 'POST':
#         this_code = request.POST.get('couponcode', None)
#         print(f'searching for record with: {this_code}.')
#         try:
#             this_ticket = Ticket.objects.get(couponcode=this_code)
#             my_form = ActivateTicketForm(request.POST, instance=this_ticket)
#         except:
#             msg = f'Code {this_code} not found.'
#             print(msg)
#             messages.add_message(request, messages.ERROR, msg)
#             add_to_list(activate_history, msg, 'error')
#         # my_form = ActivateTicketForm(request.POST, instance=this_ticket)
#         if my_form.is_valid():
#             print(my_form.cleaned_data)
#             this_notes = my_form.cleaned_data.get('activatednotes')
#             this_ticket.refresh_from_db()
#             if this_ticket.voiddate:
#                 msg = f'Ticket {this_ticket.id} ({this_ticket.couponcode}) was VOIDED on {this_ticket.voiddate}, Note: {this_ticket.voidnotes}.'
#                 print(msg)
#                 messages.add_message(request, messages.ERROR, msg)
#                 add_to_list(activate_history, msg, 'error')
#                 return HttpResponseRedirect('')
#             elif this_ticket.redeemeddate:
#                 msg = f'Ticket was already REDEEMED on {this_ticket.redeemeddate}.'
#                 print(msg)
#                 messages.add_message(request, messages.ERROR, msg)
#                 add_to_list(activate_history, msg, 'error')
#                 return HttpResponseRedirect('')
#             elif this_ticket.activateddate and this_ticket.activatednotes and not this_ticket.voiddate:
#                 msg = f'Ticket {this_ticket.id} ({this_ticket.couponcode}) was ALREADY activated on {this_ticket.activateddate}. Notes: {this_ticket.activatednotes}.'
#                 print(msg)
#                 messages.add_message(request, messages.WARNING, msg)
#                 add_to_list(activate_history, msg, 'warning')
#                 return HttpResponseRedirect('')
#             elif this_ticket.activateddate and not this_ticket.activatednotes and not this_ticket.voiddate:
#                 msg = f'Ticket ALREADY activated on {this_ticket.activateddate}.'
#                 print(msg)
#                          print(my_form.cleaned_data)
#    return HttpResponseRedirect('')
#             elif not this_ticket.activateddate and not this_ticket.redeemeddate and not this_ticket.voiddate:
#                 this_ticket.activateddate, this_ticket.activatednotes = current_timestamp, this_notes
#                 this_ticket.save(update_fields=['activateddate', 'activatednotes'])
#                 msg = f'Ticket {this_ticket.id} ({this_ticket.couponcode}) now ACTIVATED! Notes: {this_ticket.activatednotes}.'
#                 print(msg)
#                 messages.add_message(request, messages.SUCCESS, msg)
#                 add_to_list(activate_history, msg, 'success')
#                 return HttpResponseRedirect('')
#             else:
#                 print('Could not save to DB.')
#     context = {
#         'form': my_form,
#         'history': activate_history
#     }
#     return render(request, 'tickets/activate.html', context)


def bulk_activate_tickets(request, *args, **kwargs):
    my_form = ActivateTicketForm()
    if request.method == 'POST':
        this_code = request.POST.get('couponcode', None)
        print(f'searching for record with: {this_code}.')
        try:
            this_ticket = Ticket.objects.get(couponcode=this_code)
            my_form = ActivateTicketForm(request.POST, instance=this_ticket)
        except:
            msg = f'Code {this_code} not found.'
            print(msg)
            messages.add_message(request, messages.ERROR, msg)
            add_to_list(bactivate_history, msg, 'error')
            return HttpResponseRedirect('')
        # my_form = ActivateTicketForm(request.POST, instance=this_ticket)
        if my_form.is_valid():
            print(my_form.cleaned_data)
            this_notes = my_form.cleaned_data.get('activatednotes')
            this_ticket.refresh_from_db()
            try:
                if this_ticket.ticketbundle:
                    Ticket.objects.filter(ticketbundle=this_ticket.ticketbundle).update(activateddate=current_timestamp, activatednotes=this_notes)
                    msg = f'TicketGroup {this_ticket.ticketbundle} ACTIVATED. Note: {this_notes}.'
                    print(msg)
                    messages.add_message(request, messages.SUCCESS, msg)
                    add_to_list(bactivate_history, msg, 'success')
                    return HttpResponseRedirect('')
                else:
                    msg = f'Ticket {this_ticket.id} ({this_ticket.couponcode}) is not part of a bundle.'
                    print(msg)
                    messages.add_message(request, messages.ERROR, msg)
                    add_to_list(bactivate_history, msg, 'error')
            except Exception as err:
                msg = f'Error: {err}.'
                print(msg)
                messages.add_message(request, messages.ERROR, msg)
                add_to_list(bactivate_history, msg, 'error')
                return HttpResponseRedirect('')
    context = {
        'form': my_form,
        'history': bactivate_history
    }
    return render(request, 'tickets/bulkactivate.html', context)


def void_tickets(request, *args, **kwargs):
    my_form = VoidTicketForm()
    if request.method == 'POST':
        this_code = request.POST.get('couponcode', None)
        this_id = request.POST.get('id', None)
        print(f'searching for record with: {this_code}{this_id}.')
        try:
            if this_code:
                this_ticket = Ticket.objects.get(couponcode=this_code)
                my_form = VoidTicketForm(request.POST, instance=this_ticket)
                if my_form.is_valid():
                    print(my_form.cleaned_data)
                    this_notes = my_form.cleaned_data.get('voidnotes')
                    this_ticket.refresh_from_db()
            elif this_id:
                this_ticket = Ticket.objects.get(id=this_id)
                my_form = VoidTicketForm(request.POST, instance=this_ticket)
                if my_form.is_valid():
                    print(my_form.cleaned_data)
                    this_notes = my_form.cleaned_data.get('voidnotes')
                    this_ticket.refresh_from_db()
            this_ticket.voiddate, this_ticket.voidnotes = current_timestamp, this_notes
            this_ticket.save(update_fields=['voiddate', 'voidnotes'])
            msg = f'Ticket {this_ticket.id} ({this_ticket.couponcode}) VOIDED on {this_ticket.voiddate}. Note: {this_ticket.voidnotes}.'
            print(msg)
            messages.add_message(request, messages.WARNING, msg)
            add_to_list(void_history, msg, 'success')
            return HttpResponseRedirect('')
        except Exception as err:
            msg = f'Error: {err}'
            print(msg)
            messages.add_message(request, messages.ERROR, msg)
            add_to_list(void_history, msg, 'error')
            return HttpResponseRedirect('')
    context = {
        'form': my_form,
        'history': void_history
    }
    return render(request, 'tickets/void.html', context)


def settings_view(request):
    my_form = SettingsForm(request.POST or None)
    settings = Settings.objects.get(id=1)
    groupinterval = settings.groupinterval
    if request.method == 'POST':
        if my_form.is_valid():
            newinterval = int(my_form.cleaned_data.get('groupinterval'))
            Settings.objects.filter(id=1).update(groupinterval=newinterval)
            groupinterval = newinterval
    context = {
        'form': my_form,
        'groupinterval': groupinterval
    }
    return render(request, 'settings.html', context)


def bulk_create_tickets(request, *args, **kwargs):
    return render(request, 'bulkcreate.html', {})

