from django.urls import path
from .views import (
    ticket_list_view,
    ticket_detail_view,
    ticket_create_view,
    ticket_redeem,
    sell_tickets,
    # activate_tickets,
    bulk_activate_tickets,
    void_tickets,
    bulk_create_tickets,
    settings_view
)

urlpatterns = [
    path('create/', ticket_create_view, name='create'),
    path('detail/<int:tid>/', ticket_detail_view, name='detail'),
    path('all/', ticket_list_view, name='all'),
    path('redeem/', ticket_redeem, name='redeem'),
    path('sell/', sell_tickets, name='sell'),
    # path('activate/', activate_tickets, name='activate'),
    path('bulkactivate/', bulk_activate_tickets, name='bulkactivate'),
    path('void/', void_tickets, name='void'),
    path('bulkcreate/', bulk_create_tickets, name='bulkcreate'),
    path('settings/', settings_view, name='settings')
]