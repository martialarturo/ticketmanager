# Generated by Django 3.1.2 on 2020-12-31 05:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Tickets', '0003_settings'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='ticket',
            options={'managed': True},
        ),
    ]
