from django.db import models
from django.urls import reverse


class Ticket(models.Model):
    id = models.AutoField(primary_key=True)
    couponcode = models.CharField(unique=True, max_length=11, null=True) # , blank=True
    activateddate = models.DateTimeField(blank=True, null=True)
    activatednotes = models.CharField(max_length=255, blank=True, null=True)
    redeemeddate = models.DateTimeField(blank=True, null=True)
    voiddate = models.DateTimeField(blank=True, null=True)
    voidnotes = models.CharField(max_length=255, blank=True, null=True)
    ticketgroup = models.IntegerField(blank=True, null=True)
    ticketbundle = models.IntegerField(blank=True, null=True)

    class Meta:
        app_label = 'Tickets'
        managed = True
        db_table = 'tickets'

    def get_absolute_url(self):
        return reverse('detail', kwargs={'tid': self.id})


class Settings(models.Model):
    groupinterval = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'settings'