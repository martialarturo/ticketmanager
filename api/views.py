from rest_framework import viewsets
from rest_framework.decorators import action
from .serializers import TicketSerializer
from Tickets.models import Ticket


class TicketViewSet(viewsets.ModelViewSet):
    queryset = Ticket.objects.all().order_by('id')
    serializer_class = TicketSerializer
