from rest_framework import serializers

from Tickets.models import Ticket

class TicketSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Ticket
        fields = [
            'id',
            'couponcode',
            'activateddate',
            'activatednotes',
            'redeemeddate',
            'voiddate',
            'voidnotes',
            'ticketgroup'
        ]
