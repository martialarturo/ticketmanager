# README #

## About ##

This application was created to manage ticketing at a haunted house or similar event. Tickets are easily created at the point-of-sale and a station or stations at the point of entry can validate and redeem tickets on the "Redeem" page. A USB barcode reader can be used to enter couponcodes in their text input fields and those fields have auto-focus so the user can just continually scan tickets to validate. Validiating tickets ensures that ticket exists and has been properly activated, and has NOT been voided or already redeemed. Admins currently have the ability to activate promo tickets (i.e. from a printing company) en masse, void individual tickets (for stolen tickets or resold promo tickets), view all tickets, and adjust settings (like the time interval between groups).  

See it live at [TicketManager](https://agile-sands-39259.herokuapp.com){target="_blank"}
(Give it a few seconds to spin up at first, it's just on a Heroku Dyno)


## Issues ##

* Designed to run entirely locally (runs surprisingly well on Raspberry Pi 4!), there's a little DB lag on live Dyno.  
* Converted from MySQL to Postgres for deploying to Heroku  
* Whole-group feature on Redeem page broken after deploying  

## Features ##
### Current Features ###
1. Validates tickets, most importantly at Redeem (where tickets are taken and admission to event/facility is granted.
2. Flashes background colors red, yellow, or green for greater visibilty of validation result.
3. Messaging was originally handled with Toastr, but was later moved to new history module on most pages, so that user can see recent history.

### To-Do List ##
1. Rework group timer on Redeem page to not reset every page load.
2. Redeem whole group feature broken since deployment.
1. Finish pagination on 'View All' page. Enable sorting/filtering/searching.
2. Add history length control to settings page.
3. Add manager ability to create new users (replace django's built-in admin panel)
4. Configure multiple user access levels and allow admins to toggle various accesses.
5. More styling
5. Finish direct printer feature  
5. Add live statistics to admin's home view  
5. So much more...

### Pages ###

1. Sell  
    * A basic "sales" view. Selling tickets creates and activates them.  
    * Add notes (save to tickets.activatednotes field)  
    * Sell in quantities of up to 10. Will assign these ten tickets the same id in tickets.ticketgroup)  
2. Redeem  
    * Redeem valid tickets by adding current datetime to tickets.redeemeddate.  
    * Validation checks to see that tickets exist, are activated, and are not already redeemed or void.  
    * If ticket was sold in a group, user will be asked if they'd like to redeem whole group or just individual.  
    * Ticket number field was added by request because barcodes on some tickets weren't printed well and wouldn't scan.  
    * Also has a countdown timer of when to send the next group through (timer duration is set in db in settings.groupinterval)  
3. Bulk Activate (authenticated users only)  
    * For promo tickets to be issued outside of event location. These tickets have to be loaded into DB, but not yet activated.  
    * printing companies usually ship them in premade bundles. Activating any ticket on this screen activates every ticket in it's bundle.  
    * This prevents tickets from stolen or lost bundles from being used for admission, since they were never activated.  
4. Void (authenticated users only)  
    * Voids a single ticket by entering current datetime into tickets.voiddate field (and notes into ticket.voidnotes).  
    * If for any reason, a specific ticket needs to be rejected at the gate (redeem station)  
5. View All (authenticated users only)  
    * **Still a work in progress**  
    * Currently lists every ticket and it's current status  
    * has some basic pagination  
6. Settings (authenticated users only)  
    * **Still a work in progress**  
    * future admin panel  
    * can adjust the time interval displayed on Redeem page  

## Database ##

### Tickets Schema ###

| id | couponcode | activateddate | activatednotes | redeemeddate | voiddate | voidnotes | ticketgroup | ticketbundle |
| -- | ---------- | ------------- | -------------- | ------------ | -------- | --------- | ----------- | ------------ |
| 1 | 1A2B3C4D | 2020-10-31 19:30:00 | blood bank promo |  |  |  | 2 |  |
| 2 | 1234ABCD | 2020-10-15 08:00:00 |  |  | 2020-10-25 17:00:00 | Jerk selling his promo ticket on FB marketplace | 3 |  |

### Settings Schema ###
| id | groupinterval |
| -- | ------------- |
| 1 | 60 |


### Postgres functions ###

For generating a new, unique code of specified length.

~~~~sql
CREATE OR REPLACE FUNCTION public.generatecode(IN length integer)
    RETURNS character
    LANGUAGE 'plpgsql'
    VOLATILE
    PARALLEL UNSAFE
    COST 100
    
AS $BODY$
DECLARE newcouponcode VARCHAR DEFAULT '';

DECLARE chars text[] := '{A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,0,1,2,3,4,5,6,7,8,9}';
DECLARE result text := '';
DECLARE i integer := 0;

DECLARE newcode VARCHAR;
DECLARE rcount INT;

BEGIN
for i in 1..length loop
	result := result || chars[1+random()*(array_length(chars, 1)-1)];
end loop;

rcount = -1;
SELECT COUNT(*) INTO rcount FROM tickets WHERE couponcode = result;

IF rcount = 0 THEN
	newcouponcode := result;
END IF;

RETURN newcouponcode;
END
$BODY$;
~~~~
